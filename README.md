# Gitolite install
## Links
* [Gitolite home page](http://gitolite.com)
* [Gitolite on github](https://github.com/sitaramc/gitolite)

## Install
## gitolite (ubuntu)
Install and setup gitolite server.
```bash
# Cd to example
cd example

# Edit inventory
vim ./inventory/hosts

# Add your key to gitolite.yml
vim ./gitolite.yml

# Install gitolite
ansible-playbook ./gitolite.yml

# List repos
ssh git@server

# Download repo config
git clone git@server:gitolite-admin

# Add new repo
vim ./gitolite-admin/conf/gitolite.conf
# Add new strings like:
# repo myNewRepo
#     RW+     =   admin
git commit -am 'Add myNewRepo'
git push
```
